<?php
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('trangchu', ['as' => 'trang-chu', 'uses' => 'PageController@getIndex']);
Route::get('loaisanpham/{id?}', ['as' => 'loai-san-pham', 'uses' => 'PageController@getLoaiSanPham']);
Route::get('chitietsanpham', ['as' => 'chi-tiet-san-pham', 'uses' => 'PageController@getChiTietSanPham']);
Route::get('lienhe', ['as' => 'get-lien-he', 'uses' => 'PageController@getLienHe']);
Route::post('lienhe', ['as' => 'post-lien-he', 'uses' => 'PageController@postLienHe']);
Route::get('dangky', ['as' => 'getRegister', 'uses' => 'PageController@getRegister']);
Route::post('dangky', ['as' => 'postRegister', 'uses' => 'PageController@postRegister']);
Route::get('gioithieu', ['as' => 'gioi-thieu', 'uses' => 'PageController@getGioiThieu']);
Route::get('login', ['as' => 'dangnhap', 'uses' => 'PageController@getLogin']);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
