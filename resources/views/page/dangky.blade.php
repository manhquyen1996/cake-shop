	
@extends('master')

@section('content')

	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Đăng kí</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb">
					<a href="{{ route('trang-chu') }}">Home</a> / <span>Đăng kí</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div class="container">
		<div id="content">
			
			<form action="{{ route('postRegister') }}" method="post" class="beta-form-checkout">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<h4>Đăng kí</h4>
						<div class="space20">&nbsp;</div>

						
						<div class="form-block">
							<label for="email">Email address*</label>
							<input name="email" type="email" id="email">
							<p style="color:red">{{ $errors->first('email') }}</p>
						</div>
						

						<div class="form-block">
							<label for="your_last_name">Fullname*</label>
							<input name="name" type="text" id="your_last_name">
						</div>
						<p style="color:red">{{ $errors->first('name') }}</p>

						<div class="form-block">
							<label for="adress">Address*</label>
							<input name="address" type="text" id="adress" value="">
						</div>
						<p style="color:red">{{ $errors->first('address') }}</p>

						<div class="form-block">
							<label for="phone">Phone*</label>
							<input name="phone" type="text" id="phone">
						</div>
						<p style="color:red">{{ $errors->first('phone') }}</p>

						<div class="form-block">
							<label for="phone">Password*</label>
							<input name="password" type="text" id="phone">
						</div>
						<p style="color:red">{{ $errors->first('password') }}</p>

						<div class="form-block">
							<label for="phone">Re password*</label>
							<input name="re_password" type="text" id="phone">
						</div>
						<p style="color:red">{{ $errors->first('re_password') }}</p>

						<div class="form-block">
							<button type="submit" class="btn btn-primary">Register</button>
						</div>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->

@endsection