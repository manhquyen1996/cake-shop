{{--
<div class="alert alert-success">
	Bạn đã thêm thành công sản phẩm!
</div>--}}
{{--$errors->first cho phép xuất phần tử đầu tiên--}}
{{--$errors->has('...')=>cho phép ktra có lỗi ko--}}
{{--$errors->get('...')=>cho phép xuất 1 lỗi của 1 trường nào đó--}}
{{--$errors->all()=>xuất tât cả lỗi--}}
@foreach ($errors->all() as $message)
    <div class="alert alert-danger">
        {{$message}}
    </div>
@endforeach