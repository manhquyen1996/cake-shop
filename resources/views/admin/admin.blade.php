
@extends('admin.master')
@section('main')
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-success">Added user success!</div>
        <table class="table table-striped">
            <tr id="tbl-first-row">
                <td width="5%">#</td>
                <td width="30%">Fullname</td>
                <td width="25%">Username</td>
                <td width="25%">Email</td>
                <td width="5%">Level</td>
                <td width="5%">Edit</td>
                <td width="5%">Delete</td>
            </tr>

            @foreach($users as $user)


            <tr>
               <td>{{$user->user_id}}</td>
               <td>{{$user->user_fullname}}</td>
               <td>{{$user->user_name}}</td>
               <td>{{$user->user_mail}}</td>
               <td>{{$user->user_level}}</td>
               <td><a href="">Edit</a></td>
               <td><a href="" onclick="confirm('Are You Sure ?)">Delete</a></td>
            </tr>

            @endforeach
        </table>
        <div aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a aria-label="Previous" href="#">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
