<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='users';
    protected $primaryKey = 'user_id';
    protected $quarded = [];// thao tác vs tất cả các cột
    protected $hidden=[];// để ẩn hiển thị của cột mình muốn ra ngoài 

// phải use vào controller
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
