<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Product;
use App\ProductType;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\DB;
use Hash;


class PageController extends Controller
{
    //

    public function getIndex()
    {
    	$slides = Slide::all();
    	$new_products = Product::where('new', 1)->paginate(8);
    	$sale_products = Product::where('promotion_price', '>', 0)->paginate(8);


    	return view('page.trangchu', compact('slides', 'new_products', 'sale_products'));
    }
   
   
    public function getLoaiSanPham($id = 1)
    {
        $products_type = ProductType::where('id', $id)->first();
        
        $product_types = ProductType::all();
        $new_products = Product::where('id_type', $id)->paginate(6);



       return view('page.loai_san_pham', compact('product_types', 'products_type', 'new_products'));
    }

    
    public function getChiTietSanPham()
    {
        return view('page.chitiet_sanpham');
        /*$product = Product::where('id', $id)->first();
    	return view('page.chitiet_sanpham', compact('product'));*/
    }
    
    public function getLienHe()
    {
    	return view('page.lienhe');
    }
    public function postLienHe(CommentRequest $request)
    {

        DB::table('comments')->insert(
            [
                'name' => "$request->name",
                'email' => "$request->email",
                'subject' => "$request->subject",
                'comments' => "$request->message",
            ]

        );
        
        return view('page.lienhe');
    }
    
    public function getLogin()
    {
    	return view('page.login');
    }

    public function postLogin()
    {
        return view('page.login');
    }

    public function getRegister()
    {
        return view('page.dangky');
    }

    public function postRegister(RegisterRequest $request)
    {
        DB::table('users')->insert(
            [
                'full_name' => "$request->name",
                'email' => "$request->email",
                'password' => Hash::make($request->password),
                'phone' => "$request->phone",
                'address' => "$request->address",
                'remember_token' => "$request->_token",
                
            ]

        );
        
        //return view('page.trangchu');
        echo 'thanh cong';
    }

    public function getGioiThieu()
    {
    	return view('page.gioithieu');
    }
 

}
