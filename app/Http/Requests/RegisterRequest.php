<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|numeric',
            'password' => 'required',
             're_password' => 'required',
            

            //
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Vui lòng nhập đúng định dạng email',
            'email.unique' => 'Email đã tồn tại',
            'name.required' => 'Vui long nhập tên',
            'address.required' => 'Vui long nhập địa chỉ',
            'name.string' => 'Vui lòng nhập tên là chữ',
            'phone.required' => 'Vui lòng nhậ số điện thoại',
            'phone.numeric' => 'Sai định dạng',
            'password.required' => 'Vui lòng nhập password',
            're_password.required' => 'Vui lòng nhập lại password',
        ];
    }
}
