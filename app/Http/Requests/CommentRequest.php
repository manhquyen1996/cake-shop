<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|string|min:3|email|unique:comments',
            'subject' => 'required|min:3',
            'message' => 'required|min:3',
            //
        ];
    }
    public function messages()
    {
        return [
            'name.required' => "Ban vui long nhap ten",
            'email.required' => 'Ban vui long nhap email',
            'message.required' => "Ban vui long nhap eamil",
            'subject.required' => "Ban vui long nhap tieu de",
            'subject.min' => "Ban vui long nhap nhieu hon 3 ky tu",
            'name.string' => "Ban vui long nhap ten la chu",
            'email.string' => "Ban vui long nhap eamil la chu",
            'email.unique' => "Email da co nguoi su dung",
            'name.min' => "Ban vui long nhap ten nhieu hon 3 ky tu",
            'message.min' => "Ban vui long nhap nhieu hon 3 ky tu",
            'message.min' => "ban vui long nhap tin nhan nhieeuf hon 3 ky tu",
        ];
    }
}
