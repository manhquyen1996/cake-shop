<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table='users';
    
    protected $fillable = ['user_id', 'user_name', 'user_fullname', 'user_email', 'user_level', 'password'];// thao tác vs tất cả các cột
    protected $hidden=['remember_token'];// để ẩn hiển thị của cột mình muốn ra ngoài 

// phải use vào controller
    public $timestamps = false;
}
