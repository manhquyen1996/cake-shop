<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = "bills";

    public function bill_detail(){
    	return $this->hasMany('App\BillDetail', 'id_detail', 'id');

    }

    public function bill_customer(){
    	return $this->belongsTo('App\Customer', 'id_customer', 'id');

    }
}
