<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
    		
    		['user_name' =>'nguyenvana', 'user_fullname' =>'Nguyen Van A', 'user_mail' =>'nguyenvana@gmail.com', 'user_level' =>'2', 'password' =>bcrypt('123456')],
    		['user_name' =>'luuthib', 'user_fullname' =>'luu thi b', 'user_mail' =>'luuthib@gmail.com', 'user_level' =>'2', 'password' =>bcrypt('1234')],
		['user_name' =>'levanc', 'user_fullname' =>'le van c', 'user_mail' =>'levanc@gmail.com', 'user_level' =>'1', 'password' =>bcrypt('12345')]


    	]);
    }
}
